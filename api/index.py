from flask import Flask, render_template, request, Response
import json
import requests
from urllib.parse import urlencode
import yt_dlp
from flask_cors import CORS
import m3u8
app = Flask(__name__)
CORS(app)
@app.route('/')
def hello():
    return 'Hello, world'

@app.route('/ytproxy', methods=['GET'])
def ytproxy():
    url = request.args.get('url')
    headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Language": "en-us,en;q=0.5",
    "Sec-Fetch-Mode": "navigate"
    }
    payload = b'\x00\x01'
    try:
        if not url:
            raise ValueError("URL is not provided")
        response = requests.post(url, headers=headers, data=payload)
        print(response.text)
        playlist = m3u8.loads(response.text)
        for item in playlist.playlists:
            item.base_path = 'https://insta-pauser.vercel.app/ytproxymin/?url=' + item.base_path
        return Response(playlist.dumps(), content_type=response.headers['content-type'])

    except Exception as e:
        return str(e), 400
@app.route('/ytproxymin', methods=['GET'])
def ytproxymin():
    url_param = request.args.get('url', '')

    if not url_param:
        return 'Please provide a valid URL parameter.'

    ydl_opts = {}
    ydl = yt_dlp.YoutubeDL(ydl_opts)
    info = ydl.extract_info(url_param, download=False)
        
    url = info['formats'][42]['url']
    print(url)
    headers = {
        "Host": "manifest.googlevideo.com",
        "Accept": "*/*",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Content-Length": "2",
        "Referer": "https://www.youtube.com/",
        "Origin": "https://www.youtube.com",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "cross-site",
        "Connection": "keep-alive",
        "Alt-Used": "manifest.googlevideo.com",
        "Pragma": "no-cache",
        "Cache-Control": "no-cache",
        "TE": "trailers",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "en-us,en;q=0.5",
        "Sec-Fetch-Mode": "navigate"
    }
    payload = b'\x00\x01'
    try:
        if not url:
            raise ValueError("URL is not provided")
        response = requests.post(url, headers=headers,data=payload)
        return Response(response.content, content_type=response.headers['content-type'])

    except Exception as e:
        return str(e), 400
@app.route('/info')
def get_video_info():
    url_param = request.args.get('url', '')

    if not url_param:
        return 'Please provide a valid URL parameter.'

    ydl_opts = {}
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        info = ydl.extract_info(url_param, download=False)
        return json.dumps(ydl.sanitize_info(info))

def copy_header(header_name, to_headers, from_headers):
    hdr_val = from_headers.get(header_name)
    if hdr_val:
        to_headers[header_name] = hdr_val

@app.route('/', defaults={'path': ''}, methods=['GET', 'POST', 'OPTIONS'])
@app.route('/<path:path>', methods=['GET', 'POST', 'OPTIONS'])
def proxy(path=''):
    # Handle CORS preflight
    if request.method == 'OPTIONS':
        response_headers = {
            'Access-Control-Allow-Origin': request.headers.get('Origin', '*'),
            'Access-Control-Allow-Methods': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization, x-goog-visitor-id, x-origin, x-youtube-client-version, Accept-Language, Range, Referer',
            'Access-Control-Max-Age': '86400',
            'Access-Control-Allow-Credentials': 'true',
        }
        return Response('', status=200, headers=response_headers)

    url = f"https://{request.args.get('__host', 'localhost:8080')}/{path}"
    
    # Copy headers from the request to the new request
    request_headers = {
        header: value for header, value in request.headers.items()
        if header.lower() in ['range', 'user-agent']
    }
    
    # Make the request to YouTube
    fetch_res = fetch(url, method=request.method, headers=request_headers, data=request.get_data())
    
    # Construct the return headers
    headers = {
        'Access-Control-Allow-Origin': request.headers.get('Origin', '*'),
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Content-Length': fetch_res.headers.get('content-length', ''),
        'Content-Type': fetch_res.headers.get('content-type', ''),
        'Content-Disposition': fetch_res.headers.get('content-disposition', ''),
        'Accept-Ranges': fetch_res.headers.get('accept-ranges', ''),
        'Content-Range': fetch_res.headers.get('content-range', ''),
    }
    
    # Return the proxied response
    return Response(fetch_res.content, status=fetch_res.status_code, headers=headers)

def fetch(url, method='GET', headers=None, data=None):
    import requests
    headers = headers or {}
    response = requests.request(method, url, headers=headers, data=data)
    return response

@app.route('/insta')
def get_reel_info():
    url_param = request.args.get('url', '')

    if not url_param:
        return 'Please provide a valid URL parameter.'

    enable_graphql = True
    post_id = url_param
    if not enable_graphql:
        print("Instagram GraphQL API is disabled.")
        return None

    if not post_id:
        return None

    api_url = "https://www.instagram.com/api/graphql"
    headers = {
        "Accept": "*/*",
        "Accept-Language": "en-US,en;q=0.5",
        "Content-Type": "application/x-www-form-urlencoded",
        "X-FB-Friendly-Name": "PolarisPostActionLoadPostQueryQuery",
        "X-CSRFToken": "RVDUooU5MYsBbS1CNN3CzVAuEP8oHB52",
        "X-IG-App-ID": "1217981644879628",
        "X-FB-LSD": "AVqbxe3J_YA",
        "X-ASBD-ID": "129477",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-origin",
        "User-Agent": "Mozilla/5.0 (Linux; Android 11; SAMSUNG SM-G973U) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/14.2 Chrome/87.0.4280.141 Mobile Safari/537.36",
    }

    request_data = {
        "av": "0",
        "__d": "www",
        "__user": "0",
        "__a": "1",
        "__req": "3",
        "__hs": "19624.HYP:instagram_web_pkg.2.1..0.0",
        "dpr": "3",
        "__ccg": "UNKNOWN",
        "__rev": "1008824440",
        "__s": "xf44ne:zhh75g:xr51e7",
        "__hsi": "7282217488877343271",
        "__dyn": "7xeUmwlEnwn8K2WnFw9-2i5U4e0yoW3q32360CEbo1nEhw2nVE4W0om78b87C0yE5ufz81s8hwGwQwoEcE7O2l0Fwqo31w9a9x-0z8-U2zxe2GewGwso88cobEaU2eUlwhEe87q7-0iK2S3qazo7u1xwIw8O321LwTwKG1pg661pwr86C1mwraCg",
        "__csr": "gZ3yFmJkillQvV6ybimnG8AmhqujGbLADgjyEOWz49z9XDlAXBJpC7Wy-vQTSvUGWGh5u8KibG44dBiigrgjDxGjU0150Q0848azk48N09C02IR0go4SaR70r8owyg9pU0V23hwiA0LQczA48S0f-x-27o05NG0fkw",
        "__comet_req": "7",
        "lsd": "AVqbxe3J_YA",
        "jazoest": "2957",
        "__spin_r": "1008824440",
        "__spin_b": "trunk",
        "__spin_t": "1695523385",
        "fb_api_caller_class": "RelayModern",
        "fb_api_req_friendly_name": "PolarisPostActionLoadPostQueryQuery",
        "variables": json.dumps({
            "shortcode": post_id,
            "fetch_comment_count": "null",
            "fetch_related_profile_media_count": "null",
            "parent_comment_count": "null",
            "child_comment_count": "null",
            "fetch_like_count": "null",
            "fetch_tagged_user_count": "null",
            "fetch_preview_comment_count": "null",
            "has_threaded_comments": "false",
            "hoisted_comment_id": "null",
            "hoisted_reply_id": "null",
        }),
        "server_timestamps": "true",
        "doc_id": "10015901848480474",
    }
    encoded_data = urlencode(request_data)
    try:
        response = requests.post(api_url, headers=headers, data=encoded_data, timeout=1000)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:
        return ("Scrape Failed")

    if response.status_code != 200:
        return ("Scrape Failed")

    content_type = response.headers["content-type"]

    if content_type != "text/javascript; charset=utf-8":
        return ("Scrape Failed")

    response_json = response.json()

    if "data" not in response_json:
        return ("Scrape Failed")
    data = response_json["data"]["xdt_shortcode_media"]

    if not data:
        return("This post does not exist")

    if not data["is_video"]:
        return("This post is not a video")

    width, height = data["dimensions"]["width"], data["dimensions"]["height"]
    video_url = data["video_url"]

    video_json = {
        "filename": 'video_file_name(unknown)',
        "width": str(width),
        "height": str(height),
        "videoUrl": video_url,
    }

    return video_json


@app.route('/download')
def result():
    my_dict = {'phy': 50, 'che': 60, 'maths': 70}
    return "Not Implemented..."
    # return render_template('result.html', result=my_dict)
if __name__ == '__main__':
    app.run(debug=True)
